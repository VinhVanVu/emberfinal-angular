

var embermain = angular.module('embermain', [
  'ui.bootstrap',
  'ui.router',
  'ngAnimate']);

embermain.controller('navCtrl', function ($scope){
  $scope.collapseNav = function() {
    if ($(window).width() < 768) {
      $scope.navCollapsed = !$scope.navCollapsed;
    }
  };

});

embermain.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    
    $stateProvider
        
        // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/',
            templateUrl: 'views/landing.html',
            controller: 'homeCtrl'
        })
        
        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('news', {
            url: '/news',
            views: {
              '': {templateUrl: 'views/news.html'},
              'footer@news': {
                templateUrl : 'footer.html'
              }
            }   
        })

        .state('teams', {
            url: '/teams',
            views: {
              '': {templateUrl: 'views/teams.html'},
              'footer@teams': {
                templateUrl : 'footer.html'
              }
            }   
        })

        .state('about', {
            url: '/about',
            views: {
              '': {templateUrl: 'views/about.html'},
              'footer@about': {
                templateUrl : 'footer.html'
              }
            }   
        })

        .state('thanks', {
            url: '/thanks',
            views: {
              '': {templateUrl: 'views/thanks.html'}
            }   
        });
});

embermain.controller('homeCtrl', function($scope) {

});

$( document ).ready(function() {
  $('.card-back').hide()

  $(function() {
    $('.card').hover(function() { 
        $(this).find('.card-front').stop(true).hide(0);
        $(this).find('.card-back').stop(true).fadeIn(400); 

    }, function() { 
        $('.card-back').stop(true).hide(0); 
        $(this).find('.card-front').stop(true).fadeIn(400); 
    });
  });
});


